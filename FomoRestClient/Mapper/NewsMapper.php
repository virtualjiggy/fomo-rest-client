<?php

namespace FomoRestClient\Mapper;

use FomoRestClient\Model\AdditionalInformation;
use FomoRestClient\Model\News;

class NewsMapper {
    
    /**
     * @var AdditionalInformation $additionalInformation
     */
    private $additionalInformation;

    /**
     * NewsMapper constructor.
     * @param AdditionalInformation $additionalInformation
     */
    public function __construct($additionalInformation) {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @param array $source
     * @return News
     */
    public function map($source) {

        $news = new News();

        $news->setId($source['id']);
        $news->setTitle($source['title']);
        $news->setTeaser($source['teaser']);
        $news->setText($source['text']);
        $news->setPublished(boolval($source['published']));

        $news->setImage(null);
        if (isset($source['image']) === true && $this->additionalInformation !== null) {
            $news->setImage($this->additionalInformation->getImageBaseUrl() . $source['image']);
        }
        $news->setThumbnail(null);
        if (isset($source['thumbnail']) === true && $this->additionalInformation !== null) {
            $news->setThumbnail($this->additionalInformation->getImageBaseUrl() . $source['thumbnail']);
        }

        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $source['date']);
        $news->setDate(null);
        if ($date instanceof \DateTime) {
            $news->setDate($date);
        }

        $authorMapper = new AuthorMapper($this->additionalInformation);
        $news->setAuthors(array());
        if (isset($source['authors']) === true) {
            foreach ($source['authors'] as $author) {
                $news->addAuthor($authorMapper->map($author));
            }
        }

        $categoryMapper = new CategoryMapper();
        $news->setCategories(array());
        if (isset($source['categories']) === true) {
            foreach ($source['categories'] as $category) {
                $news->addCategory($categoryMapper->map($category));
            }
        }

        $keywordMapper = new KeywordMapper();
        $news->setKeywords(array());
        if (isset($source['keywords']) === true) {
            foreach ($source['keywords'] as $keyword) {
                $news->addKeyword($keywordMapper->map($keyword));
            }
        }

        $news->setRelatedNews(array());
        if (isset($source['relatedNews']) === true) {
            foreach ($source['relatedNews'] as $relatedNews) {
                $news->addRelatedNews($this->map($relatedNews));
            }
        }

        return $news;
    }

}
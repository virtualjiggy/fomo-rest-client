<?php

namespace FomoRestClient\Mapper;

use FomoRestClient\Model\Category;

class CategoryMapper {

    public function __construct() {
        
    }

    /**
     * @param array $source
     * @return Category
     */
    public function map($source) {

        $category = new Category();

        $category->setId($source['id']);
        $category->setName($source['name']);

        return $category;
    }

}
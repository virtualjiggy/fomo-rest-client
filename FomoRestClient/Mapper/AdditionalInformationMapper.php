<?php

namespace FomoRestClient\Mapper;

use FomoRestClient\Model\AdditionalInformation;

class AdditionalInformationMapper {

    public function __construct() {
        
    }

    /**
     * @param array $source
     * @return AdditionalInformation
     */
    public function map($source) {

        $additionalInformation = new AdditionalInformation();

        $additionalInformation->setLimit(null);
        if (isset($source['limit']) === true) {
            $additionalInformation->setLimit($source['limit']);
        }

        $additionalInformation->setPage(null);
        if (isset($source['page']) === true) {
            $additionalInformation->setPage($source['page']);
        }

        $additionalInformation->setPreviousPage(null);
        if (isset($source['previousPage']) === true) {
            $additionalInformation->setPreviousPage($source['previousPage']);
        }

        $additionalInformation->setNextPage(null);
        if (isset($source['nextPage']) === true) {
            $additionalInformation->setNextPage($source['nextPage']);
        }

        $additionalInformation->setTotalPages(null);
        if (isset($source['totalPages']) === true) {
            $additionalInformation->setTotalPages($source['totalPages']);
        }

        $additionalInformation->setTotalNews(null);
        if (isset($source['totalNews']) === true) {
            $additionalInformation->setTotalNews($source['totalNews']);
        }

        $additionalInformation->setImageBaseUrl(null);
        if (isset($source['imageBaseUrl']) === true) {
            $additionalInformation->setImageBaseUrl($source['imageBaseUrl']);
        }

        return $additionalInformation;
    }

}
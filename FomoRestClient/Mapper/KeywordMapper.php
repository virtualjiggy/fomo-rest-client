<?php

namespace FomoRestClient\Mapper;

use FomoRestClient\Model\Keyword;

class KeywordMapper {

    public function __construct() {

    }

    /**
     * @param array $source
     * @return Keyword
     */
    public function map($source) {

        $keyword = new Keyword();

        $keyword->setId($source['id']);
        $keyword->setName($source['name']);

        return $keyword;
    }

}
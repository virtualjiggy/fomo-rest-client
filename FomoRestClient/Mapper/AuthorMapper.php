<?php

namespace FomoRestClient\Mapper;

use FomoRestClient\Model\AdditionalInformation;
use FomoRestClient\Model\Author;

class AuthorMapper {

    /**
     * @var AdditionalInformation $additionalInformation
     */
    private $additionalInformation;

    /**
     * NewsMapper constructor.
     * @param AdditionalInformation $additionalInformation
     */
    public function __construct($additionalInformation) {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @param array $source
     * @return Author
     */
    public function map($source) {

        $author = new Author();

        $author->setId($source['id']);
        $author->setName($source['name']);
        $author->setTeaser($source['teaser']);
        $author->setText($source['text']);

        $author->setImage(null);
        if (isset($source['image']) === true && $this->additionalInformation !== null) {
            $author->setImage($this->additionalInformation->getImageBaseUrl() . $source['image']);
        }

        $author->setThumbnail(null);
        if (isset($source['thumbnail']) === true && $this->additionalInformation !== null) {
            $author->setThumbnail($this->additionalInformation->getImageBaseUrl() . $source['thumbnail']);
        }

        return $author;
    }

}
<?php

namespace FomoRestClient\Mapper;

use FomoRestClient\Model\NewsResponse;

class NewsResponseMapper {
    
    public function __construct() {

    }

    /**
     * @return NewsResponse
     */
    public function map($source) {
        
        $newsResponse = new NewsResponse();

        $additionalInformationMapper = new AdditionalInformationMapper();
        $newsResponse->setAdditionalInformation($additionalInformation = null);
        if (isset($source['additionalInformation']) === true) {
            $newsResponse->setAdditionalInformation($additionalInformation = $additionalInformationMapper->map($source['additionalInformation']));
        }

        $newsMapper = new NewsMapper($additionalInformation);
        $newsResponse->setNews(array());
        if (isset($source['news']) === true) {
            foreach ($source['news'] as $news) {
                $newsResponse->addNews($newsMapper->map($news));
            }
        }

        return $newsResponse;
    }
    
}
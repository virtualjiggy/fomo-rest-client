<?php

namespace FomoRestClient;

use FomoRestClient\Mapper\NewsResponseMapper;
use FomoRestClient\Model\NewsResponse;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

require __DIR__ . '/../vendor/autoload.php';

class FomoRestClient {

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * @var string $fomoUrl
     */
    private $fomoUrl;

    /**
     * @var string $secret
     */
    private $secret;

    /**
     * @param string $fomoUrl
     * @param string $secret
     */
    public function __construct($fomoUrl, $secret) {

        $this->fomoUrl = $fomoUrl;
        $this->secret = $secret;
        $this->httpClient = new Client(array('cookies' => true));
        $this->logger = new Logger('Logger');
        $this->logger->pushHandler(new RotatingFileHandler(__DIR__ . '/../logs/FomoRestClient', 31, Logger::ERROR));
    }

    /**
     * @param int $page
     * @param int $limit
     * @return NewsResponse
     */
    public function getNewsList($page = 1, $limit = 10, $author = null, $category = null, $keyword = null) {

        $newsResponse = new NewsResponse();

        try {
            if ($this->login() === 200) {
                $requestParams = array(
                    'query' => array(
                        'page' => $page,
                        'limit' => $limit
                    )
                );
                if (empty($author) === false) {
                    $requestParams['query']['author'] = $author;
                }
                if (empty($category) === false) {
                    $requestParams['query']['category'] = $category;
                }
                if (empty($keyword) === false) {
                    $requestParams['query']['keyword'] = $keyword;
                }
                $apiResponse = $this->httpClient->request(
                    'GET',
                    $this->fomoUrl . 'api/v1/news',
                    $requestParams
                );
                $newsResponseMapper = new NewsResponseMapper();
                $newsResponse = $newsResponseMapper->map(json_decode($apiResponse->getBody()->getContents(), true));
            }
        } catch (RequestException $e) {
            $this->logger->addError($e->getMessage(), $e->getTrace());
        } finally {
            $this->logout();
        }

        return $newsResponse;
    }

    /**
     * @param int $newsId
     * @return NewsResponse
     */
    public function getNewsEntry($newsId) {

        $newsResponse = new NewsResponse();

        try {
            if ($this->login() === 200) {
                $apiResponse = $this->httpClient->request(
                    'GET',
                    $this->fomoUrl . 'api/v1/news/' . $newsId
                );
                $newsResponseMapper = new NewsResponseMapper();
                $newsResponse = $newsResponseMapper->map(json_decode($apiResponse->getBody()->getContents(), true));
            }
        } catch (RequestException $e) {
            $this->logger->addError($e->getMessage(), $e->getTrace());
        } finally {
            $this->logout();
        }

        return $newsResponse;
    }

    /**
     * @return int
     */
    private function login() {

        $statusCode = null;

        try {
            $statusCode = $this->httpClient->request(
                'POST',
                $this->fomoUrl . 'api/v1/login',
                array(
                    'json' => json_encode(array('secret' => $this->secret))
                )
            )->getStatusCode();
        } catch (RequestException $e) {
            $this->logger->addError($e->getMessage(), $e->getTrace());
        }

        return $statusCode;
    }

    /**
     * @return int
     */
    private function logout() {

        $statusCode = null;

        try {
            $statusCode = $this->httpClient->request(
                'DELETE',
                $this->fomoUrl . 'api/v1/logout'
            )->getStatusCode();
        } catch (RequestException $e) {
            $this->logger->addError($e->getMessage(), $e->getTrace());
        }

        return $statusCode;
    }

}
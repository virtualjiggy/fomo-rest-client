<?php

namespace FomoRestClient\Model;

class NewsResponse {

    /**
     * @var \FomoRestClient\Model\AdditionalInformation
     */
    private $additionalInformation;

    /**
     * @var \FomoRestClient\Model\News[]
     */
    private $news;

    public function __construct() {

    }

    /**
     * @return \FomoRestClient\Model\AdditionalInformation
     */
    public function getAdditionalInformation() {
        return $this->additionalInformation;
    }

    /**
     * @param \FomoRestClient\Model\AdditionalInformation $additionalInformation
     */
    public function setAdditionalInformation($additionalInformation) {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @return \FomoRestClient\Model\News[]
     */
    public function getNews() {
        return $this->news;
    }

    /**
     * @param \FomoRestClient\Model\News[] $news
     */
    public function setNews($news) {
        $this->news = $news;
    }

    /**
     * @param \FomoRestClient\Model\News $news
     */
    public function addNews($news) {
        $this->news[] = $news;
    }

}
<?php

namespace FomoRestClient\Model;

class Category {

    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    public function __construct() {

    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

}
<?php

namespace FomoRestClient\Model;

class AdditionalInformation {

    /**
     * @var int $limit
     */
    private $limit;

    /**
     * @var int $page
     */
    private $page;

    /**
     * @var int $previousPage
     */
    private $previousPage;

    /**
     * @var int $nextPage
     */
    private $nextPage;

    /**
     * @var int $totalPages
     */
    private $totalPages;

    /**
     * @var int $totalNews
     */
    private $totalNews;

    /**
     * @var string $imageBaseUrl
     */
    private $imageBaseUrl;

    public function __construct() {

    }

    /**
     * @return int
     */
    public function getLimit() {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit) {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage() {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page) {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPreviousPage() {
        return $this->previousPage;
    }

    /**
     * @param int $previousPage
     */
    public function setPreviousPage($previousPage) {
        $this->previousPage = $previousPage;
    }

    /**
     * @return int
     */
    public function getNextPage() {
        return $this->nextPage;
    }

    /**
     * @param int $nextPage
     */
    public function setNextPage($nextPage) {
        $this->nextPage = $nextPage;
    }

    /**
     * @return int
     */
    public function getTotalPages() {
        return $this->totalPages;
    }

    /**
     * @param int $totalPages
     */
    public function setTotalPages($totalPages) {
        $this->totalPages = $totalPages;
    }

    /**
     * @return int
     */
    public function getTotalNews() {
        return $this->totalNews;
    }

    /**
     * @param int $totalNews
     */
    public function setTotalNews($totalNews) {
        $this->totalNews = $totalNews;
    }

    /**
     * @return string
     */
    public function getImageBaseUrl() {
        return $this->imageBaseUrl;
    }

    /**
     * @param string $imageBaseUrl
     */
    public function setImageBaseUrl($imageBaseUrl) {
        $this->imageBaseUrl = $imageBaseUrl;
    }

}
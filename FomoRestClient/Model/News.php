<?php

namespace FomoRestClient\Model;

class News {

    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var string $teaser
     */
    private $teaser;

    /**
     * @var string $text
     */
    private $text;

    /**
     * @var string $image
     */
    private $image;

    /**
     * @var string $thumbnail
     */
    private $thumbnail;

    /**
     * @var \DateTime $date
     */
    private $date;

    /**
     * @var boolean $published
     */
    private $published;

    /**
     * @var Author[]
     */
    private $authors;

    /**
     * @var Category[]
     */
    private $categories;

    /**
     * @var Keyword[]
     */
    private $keywords;

    /**
     * @var News[]
     */
    private $relatedNews;

    public function __construct() {

    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTeaser() {
        return $this->teaser;
    }

    /**
     * @param string $teaser
     */
    public function setTeaser($teaser) {
        $this->teaser = $teaser;
    }

    /**
     * @return string
     */
    public function getText() {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text) {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image) {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getThumbnail() {
        return $this->thumbnail;
    }

    /**
     * @param string $thumbnail
     */
    public function setThumbnail($thumbnail) {
        $this->thumbnail = $thumbnail;
    }

    /**
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date) {
        $this->date = $date;
    }

    /**
     * @return boolean
     */
    public function isPublished() {
        return $this->published;
    }

    /**
     * @param boolean $published
     */
    public function setPublished($published) {
        $this->published = $published;
    }

    /**
     * @return Author[]
     */
    public function getAuthors() {
        return $this->authors;
    }

    /**
     * @param Author[] $authors
     */
    public function setAuthors($authors) {
        $this->authors = $authors;
    }

    /**
     * @param Author $author
     */
    public function addAuthor($author) {
        $this->authors[] = $author;
    }

    /**
     * @return Category[]
     */
    public function getCategories() {
        return $this->categories;
    }

    /**
     * @param Category[] $categories
     */
    public function setCategories($categories) {
        $this->categories = $categories;
    }

    /**
     * @param Category $category
     */
    public function addCategory($category) {
        $this->categories[] = $category;
    }

    /**
     * @return Keyword[]
     */
    public function getKeywords() {
        return $this->keywords;
    }

    /**
     * @param Keyword[] $keywords
     */
    public function setKeywords($keywords) {
        $this->keywords = $keywords;
    }

    /**
     * @param Keyword $keyword
     */
    public function addKeyword($keyword) {
        $this->keywords[] = $keyword;
    }

    /**
     * @return News[]
     */
    public function getRelatedNews() {
        return $this->relatedNews;
    }

    /**
     * @param News[] $relatedNews
     */
    public function setRelatedNews($relatedNews) {
        $this->relatedNews = $relatedNews;
    }

    /**
     * @param News $relatedNews
     */
    public function addRelatedNews($relatedNews) {
        $this->relatedNews[] = $relatedNews;
    }

}
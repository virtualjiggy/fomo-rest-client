<?php

namespace FomoRestClient\Test;

use FomoRestClient\FomoRestClient;

class TestNews extends \PHPUnit_Framework_TestCase {

    /**
     * @var FomoRestClient $fomoRestClient
     */
    private $fomoRestClient;

    public function setUp() {

        parent::setUp();
        $this->fomoRestClient = new FomoRestClient('http://fomo.local/', 'abc123');
    }

    public function testGetNewsList() {

        $newsResponse = $this->fomoRestClient->getNewsList();
    }

    public function testGetNewsListByAuthor() {

        $newsResponse = $this->fomoRestClient->getNewsList(1, 10, 2);
    }

    public function testGetNewsListByCategoryAndKeyword() {

        $newsResponse = $this->fomoRestClient->getNewsList(1, 10, null, 2, 1);
    }


    public function testGetNewsEntry() {

        $newsResponse11 = $this->fomoRestClient->getNewsEntry(11);
        $newsResponse12 = $this->fomoRestClient->getNewsEntry(12);
        $newsResponse13 = $this->fomoRestClient->getNewsEntry(13);
    }

}
